module ROPModuleTests

open Expecto
open Expecto.Flip
open ROP

let createROPFromString isValid str =
    if isValid str then sprintf "createROPFromString Success : %s" str |> succeed
    else sprintf "createROPFromString Failure : %s" str |> createFail


[<Tests>]
let teeTests =
    testList "tee functions" [
        testCase "eitherTee on success" <| fun _ ->
            let input = "Input string"
            let expected = "createROPFromString Success : Input string + Success"
            let mutable result = ""

            let dummyOnSucess s =
                result <- sprintf "%s + Success" s
    
            let dummyOnFail s =
                result <- sprintf "%A + Fail" s

            let isValid str =
                str <> "Input"

            createROPFromString isValid input
            |> eitherTee dummyOnSucess dummyOnFail
            |> ignore

            result |> Expect.equal "Success func called" expected

        testCase "eitherTee on failure" <| fun _ ->
            let input = "Input"
            let expected = "[\"createROPFromString Failure : Input\"] + Fail"
            let mutable result = ""

            let dummyOnSucess s =
                result <- sprintf "%s + Success" s
    
            let dummyOnFail s =
                result <- sprintf "%A + Fail" s

            let isValid str =
                str <> "Input"

            createROPFromString isValid input
            |> eitherTee dummyOnSucess dummyOnFail
            |> ignore

            result |> Expect.equal "Success func called" expected

        testCase "failureTee" <| fun _ ->
            let input = "Input"
            let expected = "[\"createROPFromString Failure : Input\"] + Fail"
            let mutable result = ""

            let dummyOnSucess s =
                result <- sprintf "%s + Success" s
    
            let dummyOnFail s =
                result <- sprintf "%A + Fail" s

            let isValid str =
                str <> "Input"

            createROPFromString isValid input
            |> failureTee dummyOnFail
            |> ignore

            result |> Expect.equal "Success func called" expected

        testCase "successTee" <| fun _ ->
            // Arrange
            let input = "Input string"
            let expected = "createROPFromString Success : Input string + Success"
            let mutable result = ""

            let dummyOnSucess s =
                result <- sprintf "%s + Success" s
    
            let dummyOnFail s =
                result <- sprintf "%A + Fail" s

            let isValid str =
                str <> "Input"

            // Act
            createROPFromString isValid input
            |> successTee dummyOnSucess
            |> ignore

            // Assert
            result |> Expect.equal "Success func called" expected
        ]

[<Tests>]
let mapTests =
    testList "map functions" [
        testCase "eitherMap on success" <| fun _ ->
            // Arrange
            let input = "Input string"
            let expected = succeed "createROPFromString Success : Input string + Success"

            let dummyOnSucess s =
                sprintf "%s + Success" s
                
            let dummyOnFail f =
                [ sprintf "%A + Fail" f ] @ f

            let isValid str =
                str <> "Input"

            // Act
            let result = 
                createROPFromString isValid input
                |> eitherMap dummyOnSucess dummyOnFail

            // Assert
            result |> Expect.equal "Success func called" expected

        testCase "eitherMap on failure" <| fun _ ->
            // Arrange
            let input = "Input"
            let expected = fail [ "Dummy Fail"; "createROPFromString Failure : Input" ]

            let dummyOnSucess s =
                sprintf "%s + Success" s
                
            let dummyOnFail f =
                [ sprintf "Dummy Fail" ] @ f

            let isValid str =
                str <> "Input"

            // Act
            let result = 
                createROPFromString isValid input
                |> eitherMap dummyOnSucess dummyOnFail

            // Assert
            result |> Expect.equal "Success func called" expected
    ]


type ErrorsType1 =
| FirstErrorType1
| SecondErrorType1

type ErrorsType2 =
| FirstErrorType2
| SecondErrorType2

[<Tests>]
let utilitiesTests =
    testList "Utilities" [
        testCase "mapMessage" <| fun _ ->
            // Arrange
            let map message =
                match message with
                | FirstErrorType1 -> FirstErrorType2
                | SecondErrorType1 -> SecondErrorType2
            let input = FirstErrorType1
            let expected = createFail FirstErrorType2

            // Act
            createFail input
            |> mapMessage map
            |> Expect.equal "Error has been converted" expected

        testCase "resultToBool is Success" <| fun _ ->
            // Arrange
            let input = succeed "Success"
            let expected = true

            // Act & Assert
            resultToBool input
            |> Expect.isTrue "Success is true"
       
        testCase "resultToBool is Failure" <| fun _ ->
            // Arrange
            let input = createFail "Failure"
            let expected = false

            // Act & Assert
            resultToBool input
            |> Expect.isFalse "Failure then false"
    ]
    