module ROP

type ROPResult<'TResult, 'TMessage> =
    | Success of 'TResult
    | Failure of 'TMessage list

// Crée un succès (return)
let succeed x = Success x

// Create a new failure list from single element (return)
let createFail f = Failure [f]

// Create a new failure list from a list (return)
let fail fList = Failure fList

// Concatenate two failure lists
let addFail fList1 fList2 = fail (fList1 @ fList2)

// Pour les fonctions successFunc et failureFunc qui gèrent les 2 sorties
let either successFunc failureFunc twoTrackInput =
    match twoTrackInput with
    | Success s -> successFunc s
    | Failure f -> failureFunc f

let bind f =
    either f fail

let (>>=) x f = bind f x

// Pour les fonctions qui n'ont qu'une sortie
let successMap f =
    either (f >> succeed) fail 

let (<!>) = successMap

let failureMap f =
    either succeed (f >> fail)

// let eitherMap successFunc failureFunc twoTrackInput =
//     match twoTrackInput with
//     | Success s -> succeed (successFunc s)
//     | Failure f -> passFail (failureFunc f)
let eitherMap successFunc failureFunc =
    either (successFunc >> succeed) (failureFunc >> fail)

//**************
// tee functions
// successFunc and failureFunc functions must have () outputs
// successFunc takes 'a as input failureFunc takes 'a list input 
//**************

// let eitherTee successFunc failureFunc twoTrackInput =
//     match twoTrackInput with
//     | Success s -> successFunc s
//     | Failure f -> failureFunc f
//     twoTrackInput
let eitherTee successFunc failureFunc twoTrackInput =
    either successFunc failureFunc twoTrackInput |> ignore
    twoTrackInput

// let successTee f twoTrackInput =
//     match twoTrackInput with
//     | Success s -> f s
//     | Failure f -> ()
//     twoTrackInput
let successTee successFunc twoTrackInput =
    either successFunc ignore twoTrackInput |> ignore
    twoTrackInput

// let failureTee failureFunc twoTrackInput =
//     match twoTrackInput with
//     | Success s -> ()
//     | Failure f -> failureFunc f
//     twoTrackInput
let failureTee failureFunc twoTrackInput =
    either ignore failureFunc twoTrackInput |> ignore
    twoTrackInput

//**************
// apply family
//**************

let apply func result =
    match func, result with
    | Success f, Success r -> f r |> succeed
    | Failure err1, Failure err2 -> err1 @ err2 |> fail
    | Success _, Failure err -> err |> fail
    | Failure err, Success _ -> err |> fail

let (<*>) = apply

let combine func1 func2 addSuccessFunc addFailureFunc input  =
    match func1 input, func2 input with
    | Success s1, Success s2 -> succeed (addSuccessFunc s1 s2)
    | Failure f1, Failure f2 -> fail (addFailureFunc f1 f2)
    | Failure f, _ -> fail f
    | _, Failure f -> fail f

let (&&&&) v1 v2 =
    let addSuccess s1 s2 = (s1, s2)
    let addFailure f1 f2 = f1 @ f2
    combine v1 v2 addSuccess addFailure

let (&&&&&) v1 v2 =
    let addSuccess (s1a, s1b) s2 = (s1a, s1b, s2)
    let addFailure f1 f2 = f1 @ f2
    combine v1 v2 addSuccess addFailure

let (&&&) result1 result2 =
    match result1, result2 with
    | Success s1, Success s2 -> succeed s2
    | Failure f1, Failure f2 -> addFail f1 f2
    | Failure f, _ -> fail f
    | _, Failure f -> fail f
    
//**************
// Utilities
//**************

let flatten result =
    result |> bind id

// Convert an error type to another
let mapMessage map =
    failureMap (List.map map)

// Converts a result to a bool
let resultToBool result =
    either (fun _ -> true) (fun _ -> false) result
