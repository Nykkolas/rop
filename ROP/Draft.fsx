#load "ROPModule.fs" 

open ROP

// ****************
// Tests de &&&
//*****************
let createToto str =
    if str = "Toto" then createFail "Pas de toto"
    else sprintf "This is %s" str |> succeed

let createEntier i =
    if i = 0 then createFail "Pas de 0"
    else succeed (i - 1)

let createAutre str =
    if str = "Autre" then createFail "Pas d'autre"
    else sprintf "Oh l'autre %s" str |> succeed

type UsableTruc = { Toto: string; Entier: int }

type Truc =
    | Empty
    | Usable of UsableTruc

let addTruc truc toto i =
    match truc with
    | Empty -> createFail "Pas d'empty"
    | Usable t -> succeed <| Usable { t with Toto = toto }


// Avec 2 input : OK
let input = ("toto", 1)
let truc = Usable { Toto = "n'imp"; Entier = 6 }
//let truc = Empty

input
|> ((fun (toto, i) -> createToto toto)
    &&&& (fun (toto, i) -> createEntier i)
   )
|> bind (fun (toto, i) -> addTruc truc toto i) 

// Avec 3 : ok mais au delà ça va vraiment être dégueulasse
let input3 = ("toto", 1, "imbécile")
let truc2 = Usable { Toto = "n'imp"; Entier = 6 }

input3
|> ((fun (toto, i, autre) -> createToto toto)
    &&&& (fun (toto, i, autre) -> createEntier i)
    &&&&& (fun (toto, i, autre) -> createAutre autre)
   )
|> bind (fun (toto, i, autre) -> addTruc truc2 toto i) 

let createResults = (createToto "toto1", createEntier 3)

//*****************
//*****************


//*******************
// either / eitherMap
//*******************
open ROP

let dummyEitherTest str =
    if str = "Toto" then createFail "Pas de Toto"
    else sprintf "This is %s" str |> succeed

let dummyEitherSuccess str =
    if str = "This is toto" then succeed str
    else createFail "This is NOT toto"

let dummyDisplayError e =
    printf "fail"
    e

dummyEitherTest "toto"
|> either succeed (dummyDisplayError >> fail)
// |> eitherMap (sprintf "%s") (fun f -> printfn "fail")
|> bind dummyEitherSuccess


//*******************
//*******************

//*******************
// D'autres tests avec &&& en tant que validateur
//*******************


type MyString = MyString of string

let isValidStr str =
    if str <> "input" then succeed str
    else sprintf "isValidStr invalide : %s" str |> createFail

let createMyStr str =
    isValidStr str
    |> successMap MyString

type MyInt = MyInt  of int

let isValidInt i =
    if i > 0 then succeed i
    else sprintf "isValidInt invalide : %i" i |> createFail

let createMyInt i =
    isValidInt i
    |> successMap MyInt

type Combo = {
    Name: MyString;
    Number: MyInt
}

let createCombo str i =
    if (str, i) <> (MyString "combo", MyInt 7) then succeed { Name = str; Number = i }
    else sprintf "createCombo error : %A" (str, i) |> createFail

let tupleValues = ("input", 7)

let (str5, i5) = tupleValues
(isValidStr str5) &&& (createMyInt i5)
>>= createCombo (MyString str5)

type InfiniteCombo = {
    Name: MyString;
    Number: MyInt;
    Combo: MyInt
}
let createInfiniteCombo str i j =
    if (str, i) <> (MyString "combo", MyInt 7) then succeed { Name = str; Number = i; Combo = j }
    else sprintf "createCombo error : %A" (str, i, j) |> createFail

let infiniteCombo = ("input", 0, 3)

let (infiniteStr, infiniteNumber, infiniteComboInt) = infiniteCombo
(isValidStr infiniteStr) &&& (isValidInt infiniteNumber) &&& (createMyInt infiniteComboInt)
>>= createInfiniteCombo (MyString infiniteStr) (MyInt infiniteNumber)


//*******************
//*******************
